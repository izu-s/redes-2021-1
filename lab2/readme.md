![UNAM](https://gitlab.com/nehnemini/redes-2021-1/-/raw/87fe32dd92a47c8c9ac44d46a33351f47c033927/img/img_logoFC_2019.png)
# Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información




## Practica 2: Servidor web en AWS EC2

## Objetivo:
El alumno instalará el servidor web Apache HTTPD, y publicará una formulario HTML en este servidor.
Además, se familiarizará con el uso de los servicios *VPC*, *EC2*, *Security group*, y *Elastic IP*, proporcionados por AWS.

## Introducción
El protocolo HTTPS (HyperText Transfer Protocol Secure) es un protocolo de la Capa de aplicación que
permite el envió de información cifrada usando los protocolos HTTP y SSL/TLS. El protocolo HTTP envía
información en claro a través del medio, el protocolo SSL/TLS es el encargado de encapsular el protocolo
HTTP para ser enviado de manera cifrada.

## Desarrollo

### Video

El video se encuentra en la siguiente liga: https://youtu.be/Fct2h12OJeA

### Pasos generales:

1. Crear una VPC de acuerdo a la disponibilidad por región.
3. Dentro de esa VPC, crear una instancia de Ubuntu 20 LTS.
4. Conectarse remotamente por SSH a la instancia recién creada, e instalar el servidor web Apache, y crear un formulario HTML en donde se solicite un usuario y contraseña. Este formulario será el que se muestre al acceder a la página web.
5. Solicitar una Elastic IP y asociarla a la instancia recién creada.
6. Ingresar desde un navegador web al servidor web recién creado.
7. Apagar las instancias para evitar el consumo de créditos.



## Evaluación
1. ¿Qué es el concepto de nube y a qué se refiere el término Iaas? (describir con sus propias palabras aunque sea un renglón).
2. ¿Qué ventajas observas al utilizar la infraestructura que utilizamos en esta práctica? (describir con sus propias palabras aunque sea un renglón).
3. Colocar comentarios sobre la práctica (facilidad de ejecución, valoración de lo aprendido).


### Notas adicionales
1. El reporte se entrega de manera individual.
2. Registrar en el reporte los pasos que sean considerados necesarios para explicar cómo se realizó la práctica, incluir capturas de pantalla que justifiquen los resultados obtenidos.
3. Incluir las respuestas del Cuestionario en el reporte.
4. Se pueden agregar posibles errores, complicaciones, opiniones, críticas de la práctica o del laboratorio, o cualquier comentario relativo a la práctica.
5. Subir los archivos relacionados con la práctica a Moodle en formato pdf

